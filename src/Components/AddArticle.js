import React, { useState  ,useCallback  } from 'react';
import {useDispatch} from 'react-redux';
import {addArticle} from '../redux/actions/actions';


export const AddArticleForm = props => {
  const dispatch = useDispatch()
  
  const [title, setTitle] = useState('');
  const [author, setAuthor] = useState('');
  const [category, setCategory] = useState('');
  const [article, setArticle] = useState('');

  const addNewArticle = useCallback(
    (article) => dispatch(addArticle(article)),
    [dispatch]
  )


    const mySubmitHandler = event => {

        event.preventDefault();
        if(title==='' || author==='' || category === '' ||article==='' ){
          alert("Form wasn't completely filled! Try again!")
        }
        else{
          const articleToAdd = {
            title : title,
            author: author,
            category: category,
            article: article
          }
          addNewArticle(articleToAdd);
          setArticle('');
          setAuthor('');
          setCategory('');
          setTitle('');
        }

      }
      
     const selectChangeHandler = event =>{
       if(event.target.value.trim()===''){
         alert("Empty category!");
       }
       else{
        setCategory(event.target.value);
       }
        
      }
      const titleHandler = event => {
        if(event.target.value.trim()===''){
          alert("Empty title!");
        }
        else{
          setTitle(event.target.value);
        }
        
      }
      const authorHandler = event =>{
        setAuthor(event.target.value);
      }
      const articleHandler = event =>{
        setArticle(event.target.value);
      }
  
        return ( 
            <div>
            <form onSubmit={mySubmitHandler}>
              <h1>Add new article:</h1>
              <p>Enter author:</p>
              <input
                type='text'
                name='author'
                onChange={authorHandler}
                placeholder=''
              />
              <p>Enter title:</p>
              <input
                type='text'
                name='title'
                onChange={titleHandler}
                placeholder=''
              />
              <p>Select category:</p>
              <select value={category}  onChange={selectChangeHandler}>
              <option value=""></option>
                <option value="Fashion">Fashion</option>
                <option value="Politics">Politics</option>
              </select>
              <p>Enter article:</p>
              <input
                type='text'
                name='article'
                onChange={articleHandler}
              />
              <input
                type='submit'
              />
            </form>
           </div>
         );
    
}

