import React, { useContext ,useCallback  } from 'react';
import {useDispatch} from 'react-redux';
import ThemeContext from '../contexts/Context';
import {deleteArticle}  from '../redux/actions/actions';
import Button from 'react-bootstrap/Button';

export const AtricleInfo = props => {

    const dispatch = useDispatch()
    const  themeContext  = useContext(ThemeContext);
    const deleteThisArticle =  useCallback(
        (id) => dispatch(deleteArticle(id)),
        [dispatch]
      )
    const DeleteMe = (id,e) => {
        deleteThisArticle(id);
    }
    
    const {isLightTheme, light, dark} = themeContext;
    const theme = isLightTheme ? light : dark;
    
    return ( 
        <div className="article" style={{color: theme.syntax, background:theme.bg}}>
            <p><strong>{props.article.title}</strong> by {props.article.author} </p>
            <p>{props.article.article}</p>
            <div>
              <a href={props.article.url}>Link to article</a>
            </div>
            <br></br>
            <img style={{width:'500px' }} src={props.article.image}/>
            <br></br>
            <div>
                <Button variant="outline-danger" onClick={(e)=> DeleteMe(props.article.id, e)}>
                    Delete
                </Button>
            </div>
           
        </div>
    );
    
}
