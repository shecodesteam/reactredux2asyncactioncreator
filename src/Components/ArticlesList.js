import React, {useContext ,useCallback, useEffect  } from 'react';
import ThemeContext  from '../contexts/Context';
import {AtricleInfo} from './ArticleInfo';
import { startFetchingSrticles, receivedArticles, receivedError, searchArticlesByCategory} from '../redux/actions/actions';
import {useDispatch} from 'react-redux';
import { useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import axios from 'axios';
import Loader from 'react-loader-spinner'

export const ArticlesList = props => {

  const articles = state => state.articles.articles
  
  console.log('ArticlesList loading');
  const categoryFilter = useSelector(state => state.categoryFilter);

  const dispatch = useDispatch()

  useEffect(() => {
    console.log('useEffect categoryFilter');
    //fetchArticles();
    changeTheFilterCategory(categoryFilter);

  },[]);


  const changeTheFilterCategory = useCallback(
    
    (newCategory) => dispatch(searchArticlesByCategory(newCategory)),
    [dispatch]
  )

  const selectArticlesByCategory = createSelector(
    articles,
    articles => { 
      console.log('selectArticlesByCategory',articles);
      return articles.filter(article => article.category === categoryFilter);
    }
   
  )
  const fetchArticles = ()=>{
      dispatch(startFetchingSrticles(true))
      axios.get('http://newsapi.org/v2/top-headlines?' +
      'country=us&sources?category='+categoryFilter+'&' +
      'apiKey=d1c7246186ff44abb23e9b124babfa73')
      .then(response=>{
        console.log('fetch ok',response);

        const newArticles = response.data.articles.map((article,index) => {
          return {
            id :index,
            title: article.title, 
            author: article.author, 
            category: categoryFilter, 
            article: article.description}})
        
        console.log('fetch ok articles',newArticles);

        console.log('dispatching...');

        dispatch(receivedArticles(newArticles))
      })
      .catch(error => {
        console.log('ajax error :( ',error);
        dispatch(receivedError(error.message))
      })
  }

 
  const  themeContext  = useContext(ThemeContext);

  const changeFilterCategory=(filter, e) => {
    console.log("click changeFilterCategory filter:" , filter);
      changeTheFilterCategory(filter);
  }

    
  const theme = themeContext.isLightTheme ? themeContext.light : themeContext.dark;
  const articlesByCategory = useSelector(selectArticlesByCategory);
      return ( 

        <div className="container" style={{color: theme.syntax, background:theme.ui}}>
 
          <div>
            <button className="button" onClick = {(e) => changeFilterCategory("Entertainment", e)}>
                Entertainment
            </button>
            <button className="button" onClick = {(e) => changeFilterCategory("Health",e)}>
                Health
            </button>

          </div>

          <h1  style={{color: '#555'}}>New articles about {categoryFilter}</h1>
          {
            articlesByCategory && articlesByCategory.length && categoryFilter
              ? articlesByCategory.map((article, index) => {
                 
                  return (<AtricleInfo article={article} key={index} />);
                })
              : 
              <Loader
              type="Puff"
              color="#00BFFF"
              height={100}
              width={100}
              timeout={3000} //3 secs
      
           />
           
              
              
          }
        
        </div>
      );
    
}

export default ArticlesList