import axios from 'axios';
export function setCategoryFilter(newCategory){
   return {
        type:'CHANGE_CATEGORY_FILTER',
        categoryFilter: newCategory
    };
}
export function addArticle(article){
    return{
        type:'ADD_ARTICLE',
        newArticle : article
    };
}
export function deleteArticle(id){
    return{ 
        type:'DELETE_ARTICLE',
        id: id
    };
}
export function startFetchingSrticles(payload){
    return{
        type: 'FETCH_ARTICLES_START',
        payload: payload
    }
}
export function receivedError(error){
    return{
        type:'FETCH_ARTICLES_ERROR',
        payload: error
    }
}
export function receivedArticles(articles){
    return{
        type:'RECEIVE_ARTICLES',
        payload: articles
    }
}

export const searchArticlesByCategory = (newCategory) => {
    console.log("in searchArticlesByCategory newCategory: ", newCategory)
    return function(dispatch){
        console.log('searchArticlesByCategory functiom start ',{newCategory:newCategory });
        dispatch(setCategoryFilter(newCategory))
        dispatch(startFetchingSrticles(true))
        
        var url='http://newsapi.org/v2/top-headlines?' +
        'country=us&category='+newCategory+'&' +
        'apiKey=d1c7246186ff44abb23e9b124babfa73';
        
        console.log('searchArticlesByCategory functiom start ',{url:url });
        

        return axios.get(url)
        .then(response=>{
        
  
          const newArticles = response.data.articles.map((article,index) => {
            return {
              id :index,
              title: article.title, 
              author: article.author, 
              category: newCategory, 
              article: article.content,
              image: article.urlToImage,
              url: article.url
            }})
          
        
  
          console.log('dispatching...');
  
          dispatch(receivedArticles(newArticles))
        })
        .catch(error => {
          console.log('ajax error :( ',error);
          dispatch(receivedError(error.message))
        })
    }

}
