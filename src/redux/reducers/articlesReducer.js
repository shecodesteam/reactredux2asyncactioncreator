
import initState from './initialState';


const articlesReducer = (state = initState , action) => {
    
    switch (action.type) {
        
        case 'DELETE_ARTICLE':

            let articles = state.articles.filter(article => { return  article.id !== action.id } );

            console.log('articlesReducer after delete',{
                articles:articles
            });

            return Object.assign({}, state, {
                articles: articles
        
            })
        
           
        case 'ADD_ARTICLE':
            
            return Object.assign({}, state, {

                articles: [
                  ...state.articles,
                  {
                    id : action.newArticle.id,
                    title : action.newArticle.title,
                    author: action.newArticle.author,
                    category: action.newArticle.category,
                    article: action.newArticle.article
                  }
                ]
        
              })

        case 'FETCH_ARTICLES_START':

            return Object.assign({}, state, {
                fetching: action.payload,
                fetched: false,
                error:''
              })
            
        case 'FETCH_ARTICLES_ERROR':{
         
            return Object.assign({}, state, {
                fetching: false,
                fetched:false,
                error:action.payload
              })
            
        }
        case 'RECEIVE_ARTICLES':{

         
            return{
                ...state,
                fetching: false,
                fetched: true,
                articles: action.payload,
            }
        
            
        }
            
        default:
            return state
    }
    
}
export default articlesReducer;