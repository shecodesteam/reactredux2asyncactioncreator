import categoryReducer from './categoryReducer';
import articlesReducer from './articlesReducer';
import {combineReducers} from 'redux';

const allReducers = combineReducers({
    categoryFilter: categoryReducer,
    articles: articlesReducer,
  
});
export default allReducers;